package universe

// Universe struct describing the game universe for our 4x game
type Universe struct {
	// The current state of the universe
	state [][]int
	// The size of the universe
	size int
}
