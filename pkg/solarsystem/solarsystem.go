package solarsystem

// SolarSystem is a solar system in our universe
type SolarSystem struct {
	Name    string
	Star    StarType
	Planets []PlanetType
}

// Star is a star in the game universe
type StarType struct {
	Name string
	X, Y float64
}

// Planet is a planet in the game universa
type PlanetType struct {
	Name       string
	Owner      string
	Population int
	Production int
	Research   int
	Defense    int
	Shields    int
	ShieldsMax int
}
