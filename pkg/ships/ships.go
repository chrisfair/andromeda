package ships

// Ship represents a spaceship in our game universe
type Ship struct {
	Name            string
	Owner           string
	Class           string
	SpeedSublight   int
	SpeedLight      int
	Age             int
	Attack          int
	Defense         int
	Damage          int
	Armor           int
	NumberOfAttacks int
}

// Fleet is a collection of ships
type Fleet struct {
	Ships []Ship
	Owner string
}
